const electron = require('electron');
const {
    ipcRenderer
} = electron;

const {
    dialog,
    getCurrentWindow
} = electron.remote;

var currentUser = null;
const curUs = document.querySelector('#currentUser');

ipcRenderer.on("data:user", (e, arg) => {
    // let has = JSON_parse(arg);
    // console.log(arg.username);
    if (arg.length === 0) {
        let opt = {
            type: "warning",
            title: "Warning",
            message: "User tidak terdeteksi",
            buttons: ['&Retry', '&Non User', 'Cancel']
        }
        dialog.showMessageBox(null, opt).then(konfirmasi => {
            // console.log(konfirmasi.response);
            switch (konfirmasi.response) {
                case 0:
                    ipcRenderer.send("button:new", "new");
                    break;
                case 1:
                    //   getCurrentWindow().reload();
                    currentUser = "Anonim";
                    // console.log(currentUser);
                    curUs.innerHTML = currentUser;
                    document.querySelector('#itemInput').removeAttribute('disabled');
                    document.querySelector('#itemInput').setAttribute('placeholder', 'Masukan barcode item');
                    break;
            }
        });
    } else {
        currentUser = arg[0].username;
        curUs.innerHTML = "- " + currentUser + " -";
        document.querySelector('#itemInput').removeAttribute('disabled');
        document.querySelector('#itemInput').setAttribute('placeholder', 'Masukan barcode item');
    }
})

// curUs.innerHTML = "asfas";

var formatter = new Intl.NumberFormat('en-ID');

let cart = [];
let checkoutTotal = 0;

if (document.querySelector('#totalBelanja1').innerHTML == 'Rp. 0') {
    document.querySelector('#ckh').setAttribute('disabled', '');
}

function sendClick(arg) {
    var param = "true";
    //console.log(arg);
    switch (arg) {
        case 1:
            ipcRenderer.send("button:new", param);
            break;
        case 2:
            ipcRenderer.send("button:inven", param);
            break;
        case 3:
            ipcRenderer.send("button:list", param);
            break;
        case 4:
            ipcRenderer.send("button:help", param);
            break;
    }
}

const barc = document.querySelector('#itemInput');
barc.addEventListener('keydown', inputBarcode);

function inputBarcode(arg) {
    if (arg.code == "Enter") {
        ipcRenderer.send("KodeBarcode", barc.value);
    }
}

const lastHarga = document.querySelector('#itemLast');
ipcRenderer.on("data:json", function (e, arg) {
    // console.log(typeof(arg));
    let notFound = true;
    let totalBelanja = 0;
    if (arg.length == 0) {
        dialog.showErrorBox("Missing Item", "Item tidak ditemukan !!");
        barc.value = "";
        barc.focus();
    } else {
        if (cart.length == 0) {
            cart.push({
                id: arg[0].id,
                barcode: arg[0].barcode,
                nama: arg[0].nama,
                jumlah: 1,
                harga: arg[0].harga,
                subtotal: 1 * arg[0].harga
            });
        } else {
            for (var i = 0; i <= cart.length - 1; i++) {
                if (arg[0].id == cart[i].id) {
                    cart[i].jumlah = cart[i].jumlah + 1;
                    cart[i].subtotal = cart[i].jumlah * cart[i].harga;
                    notFound = false;
                }
            }
            if (notFound) {
                cart.push({
                    id: arg[0].id,
                    barcode: arg[0].barcode,
                    nama: arg[0].nama,
                    jumlah: 1,
                    harga: arg[0].harga,
                    subtotal: 1 * arg[0].harga
                });
            }
        }
        lastHarga.innerHTML = 'Rp. ' + formatter.format(arg[0].harga);
        barc.value = "";
    }
    //console.log(cart);

    html = "<table><tr><td>#</td><td>Nama</td><td>Jumlah</td><td>Harga</td><td>Total</td></tr>";
    for (var i = 0; i < cart.length; i++) {
        html += "<tr><td>" + cart[i].id + "</td><td>" + cart[i].nama + "</td><td>" + cart[i].jumlah + "</td><td>" + 'Rp. ' + formatter.format(cart[i].harga) + "</td><td>" + 'Rp. ' + formatter.format(cart[i].harga * cart[i].jumlah) + "</td>";
        totalBelanja = totalBelanja + cart[i].harga * cart[i].jumlah;
    }
    html += "</tr></table>";
    document.querySelector('#keranjangBelanja').innerHTML = html;

    const labelHarga = document.querySelector('#totalBelanja1');
    labelHarga.innerHTML = 'Rp. ' + formatter.format(totalBelanja);
    document.querySelector('#totalBelanja2').innerHTML = 'Rp. ' + formatter.format(totalBelanja);
    checkoutTotal = totalBelanja;

    if (document.querySelector('#totalBelanja1').innerHTML == 'Rp. 0') {
        document.querySelector('#ckh').setAttribute('disabled', '');
    } else {
        document.querySelector('#ckh').removeAttribute('disabled');
    }
});

function Checkout() {
    ipcRenderer.send('checkout', {
        checkoutTotal,
        cart,
        currentUser
    });
}