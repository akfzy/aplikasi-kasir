const electron = require('electron');
const url = require('url');
const path = require('path');
const request = require('request');
var mysql = require('mysql');

var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'VbsMac81',
    database: 'smart-receipt'
});

const {
    app,
    BrowserWindow,
    ipcMain,
    globalShortcut
} = electron;

let mainWindow;
let inventoryWindow;
let checkoutWindow;
let listWindow;

app.on('ready', function () {
    const ret = globalShortcut.register('CommandOrControl+R', () => {
        mainWindow.reload();
        setTimeout(() => {
            connection.connect(function (err) {
                if (err) {
                    console.log(err.code);
                    console.log(err.fatal);
                }
            });

            $query = "UPDATE `cashier_sessions` SET `status` = '0' WHERE `cashier_sessions`.`store` = 1;";

            connection.query($query, function (err, rows, fields) {
                if (err) {
                    console.log("An error ocurred performing the query.");
                    console.log(err);
                    return;
                }
            });
        }, 1000);
    });
    if (!ret) {
        console.log('registration failed')
    }

    // Check whether a shortcut is registered.
    console.log(globalShortcut.isRegistered('CommandOrControl+X'))
    mainWindow = new BrowserWindow({
        height: 625,
        resizable: false,
        webPreferences: {
            nodeIntegration: true
        },
        title: "Kasir Amelia"
    });
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'material', 'mainWindow.html'),
        protocol: 'file',
        slashes: true
    }));
    mainWindow.removeMenu();
    mainWindow.on('closed', function () {
        globalShortcut.unregisterAll()
        app.quit();
    });
});

ipcMain.on('KodeBarcode', function (e, arg) {
    connection.connect(function (err) {
        // in case of error
        if (err) {
            console.log(err.code);
            console.log(err.fatal);
        }
    });

    $query = "SELECT * FROM `inventory_amelia` WHERE barcode='" + arg + "'";

    connection.query($query, function (err, rows, fields) {
        if (err) {
            console.log("An error ocurred performing the query.");
            console.log(err);
            return;
        }
        mainWindow.webContents.send('data:json', rows);
    });

});

ipcMain.on('data:inventory', function (e, arg) {
    connection.connect(function (err) {
        if (err) {
            console.log(err.code);
            console.log(err.fatal);
        }
    });

    $query = "INSERT INTO `inventory_amelia`(`nama`, `barcode`, `harga`, `jumlah`) VALUES ('" + arg[0].nama + "','" + arg[0].barcode + "'," + arg[0].harga + "," + arg[0].jumlah + ")";

    connection.query($query, function (err, rows, fields) {
        if (err) {
            inventoryWindow.webContents.send('alert', 400);
            return;
        }
        inventoryWindow.webContents.send('alert', 200);
    });

});

ipcMain.on('button:new', (e, arg) => {
    mainWindow.reload();
    setTimeout(() => {
        connection.connect(function (err) {
            if (err) {
                console.log(err.code);
                console.log(err.fatal);
            }
        });

        $query = "select users.username from cashier_sessions JOIN users ON users.id = cashier_sessions.user WHERE store=1 AND status=1";

        connection.query($query, function (err, rows, fields) {
            if (err) {
                console.log("An error ocurred performing the query.");
                console.log(err);
                return;
            }
            mainWindow.webContents.send('data:user', rows);
        });

    }, 1000);
});

ipcMain.on('button:inven', function (e, arg) {
    inventoryWindow = new BrowserWindow({
        height: 300,
        width: 400,
        resizable: false,
        webPreferences: {
            nodeIntegration: true
        },
        title: "Inventory",
        parent: mainWindow,
        modal: true
    });
    inventoryWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'material', 'inventoryWindow.html'),
        protocol: 'file',
        slashes: true
    }));
    inventoryWindow.removeMenu();
    inventoryWindow.on('close', function () {
        inventoryWindow = null;
    });
});

ipcMain.on('button:list', function (e, arg) {
    listWindow = new BrowserWindow({
        height: 500,
        resizable: false,
        webPreferences: {
            nodeIntegration: true
        },
        parent: mainWindow,
        modal: true
    });
    listWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'material', 'listitems.html'),
        protocol: 'file',
        slashes: true
    }));
    // listWindow.removeMenu();
    listWindow.on('close', function () {
        listWindow = null;
    });
});

ipcMain.on('checkout', (e, arg) => {
    if (arg <= 0) {
        return;
    }
    checkoutWindow = new BrowserWindow({
        height: 300,
        width: 400,
        resizable: false,
        webPreferences: {
            nodeIntegration: true
        },
        title: "Checkout",
        parent: mainWindow,
        modal: true
    });
    checkoutWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'material', 'checkoutWindow.html'),
        protocol: 'file',
        slashes: true
    }));
    checkoutWindow.removeMenu();
    checkoutWindow.on('close', function () {
        checkoutWindow = null;

        mainWindow.reload();
        connection.connect(function (err) {
            if (err) {
                console.log(err.code);
                console.log(err.fatal);
            }
        });

        $query = "UPDATE `cashier_sessions` SET `status` = '0' WHERE `cashier_sessions`.`store` = 1;";

        connection.query($query, function (err, rows, fields) {
            if (err) {
                console.log("An error ocurred performing the query.");
                console.log(err);
                return;
            }
        });
    });

    checkoutWindow.webContents.on('did-finish-load', () => {
        checkoutWindow.webContents.send('totalBelanja', arg);
    });
});

function uploadResi(passarg, callback_) {
    const options = {
        url: 'https://api-smartreceipt.herokuapp.com/Receipts',
        method: 'POST',
        json: true,
        headers: {
            'Content-type': 'application/json',
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwiaWF0IjoxNTkyNjQxMTQ1LCJleHAiOjE1OTUyMzMxNDV9.TGWXtg5BIEN82574RUj2s2axlL6NtBGnnFB4sB4Pj04'
        },
        body: {
            "market": "Amelia Fotocopy Alangamba",
            "client": passarg.user,
            "DataResi": passarg.resi
        }
    };

    request(options, function (err, res, body) {
        console.log(res);
    });
    // console.log('1');
    callback_();
}

function notifyWeb(pas) {
    uploadResi(pas, () => {
        //Callback Notifikasi
        const options = {
            url: 'http://smart-receipt.local/MidlewareAPI/Notifikasi/' + pas.user
        };

        // // console.log(Options);

        request(options, function (err, res, body) {
            console.log(res);
        });
        // console.log("2");
    });
}

ipcMain.on('Resi', (e, arg) => {
    notifyWeb(arg);
});